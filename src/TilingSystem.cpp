#include "TilingSystem.h"

TilingSystem::TilingSystem()
{
    //ctor
  xoffset = 0;
  yoffset = 0;
}

void MapLayer::setData(int data[])
{
  //tile_map = vector<int>(data, data + sizeof data / sizeof data[0]);
  int *last = &data[width*height];
  
  tile_map = vector<int>(&data[0], last);
}

void MapLayer::setData(vector<int> d)
{  
  tile_map = d;
  
}

/**
 * Returns the screenpos of the topmost tilepixel given the tile
 **/
SE_Vector2d TilingSystem::calcTileScreenPos(int x, int y){
   int tile_width, tile_height;  tile_width = 64;  tile_height = 32;
  int tx = (x - y) * tile_width / 2;
  int ty = (x + y) * tile_height /2;
  tx += xoffset;
  ty += yoffset;
  return SE_Vector2d{tx,ty};
}

/**
 * Return the tilepos given the screen cordinates
 **/
SE_Vector2d TilingSystem::calcScreenTilePos(int sx, int sy){
  sx -= xoffset;
  sy -= yoffset;
  int tile_width, tile_height;  tile_width = 64;  tile_height = 32;
  
  int tw = tile_width/2;
  int th = tile_height/2;
  int tx = ( (sx / tw) + (sy / th) ) / 2;
  int ty = ( (sy / th) - (sx / tw) ) /2;
  
  return SE_Vector2d{tx,ty};
}

/**
 * Global tile size
 * Can be different from layer tile size
 **/
void TilingSystem::setTileSize(int w, int h)
{
  tile_width = w;
  tile_height = h;
}

void TilingSystem::setMapSize(int w, int h){
  map_width = w;
  map_height=h;
}
  


int MapLayer::getTileId(int x, int y)
{

  return tile_map[y * width + x];
  
}

#include <sstream>
string MapLayer::toString()
{
  stringstream ss;
  
  ss << "name: " <<name <<endl;
  ss << "Layer Width: " <<getWidth() <<endl;
  ss << "Layer Height: " <<getHeight() << endl;
  ss << "x: " <<getXoffset() << endl;
  ss << "y: " <<getYoffset() << endl;
  ss << "Tile Width: " << getTileWidth() << endl;
  ss << "Tile Height: " << getTileHeight() << endl;
  ss << endl;
  
  return ss.str();  
}
