#include "IrrSprite.h"

IrrSprite::IrrSprite(ITexture* tex)
{    
    texture = tex;
    //this->clip = clip;
}

void IrrSprite::addFrame(core::rect<s32> clipping){
    frames.push_back(clipping);
}

void IrrSprite::draw(video::IVideoDriver* driver,
                     int frame,
                     int x, int y
                     )
{
      driver->draw2DImage(texture,
                  position2d<s32>(x,y),
                  frames[frame],
                  0,
                  video::SColor(255, 255, 255, 255),
                  true
            );
}
