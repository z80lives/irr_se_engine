#include "LevelReader.h"
#include <iostream>
#include <fstream>

#include <stdexcept>

using namespace std;

/*static string readFile(string filename){
    ifstream fs(filename);
    string str;
    fs.seekg(0, std::ios::end);
    str.reserve(fs.tellg());
    fs.seekg(0, std::ios::beg);
    str.assign( (istreambuf_iterator<char>(fs)), (istreambuf_iterator<char>()));
    fs.close();
    return str;
}*/


LevelReader::LevelReader(string dir,  string filename)
{
    this->dir = dir;
   ifstream i(dir+filename);
   if(!i.is_open())
     throw std::runtime_error("File "+ filename+" not found in directory "+dir+".");
   jsonData << i;
}

void LevelReader::loadTileset(Tileset &ts, string filename){
  ifstream i(filename);
   if(!i.is_open())
     throw std::runtime_error("File "+ filename+" not found.");
   json j;
   j << i;

   ts.columns = j["columns"];
   ts.filename = j["image"];

   ts.width = j["imagewidth"];
   ts.height = j["imageheight"];
   ts.margin = j["margin"];
   ts.spacing = j["spacing"];
   ts.tilecount = j["tilecount"];
   ts.tileheight = j["tileheight"];
   ts.tilewidth = j["tilewidth"];
   //ts.transparent_color = j["transparentcolor"];

   i.close();
}

void LevelReader::loadLevel(TilingSystem& ts){
  int h = jsonData["height"];
  int w = jsonData["width"];
  int tw = jsonData["tilewidth"];
  int th = jsonData["tileheight"];
  ts.setMapSize(w, h);
  ts.setTileSize(tw, th);

  //load tilesets
  for(json tileset : jsonData["tilesets"]){
    Tileset t;
    string path = tileset["source"];
    loadTileset(t, dir+ path);
    ts.addTileset(t);
    //read columns
    //int w ,h; w=h=0;
    //w= tilset["imagewidth"]
    //h= tilset["imageheight"]
    //TileSet *tset = new Tileset(&ts);
    //tset->setColumns(4);
    //tset->imageSize(w, h);
    //read tilecount
  }

  //  json layers_json = jsonData["layers"];
  for(json d : jsonData["layers"]  ){
      int lw=w, lh=h;
      string type = "";
      type = d["type"] ;

      if(type == "tilelayer"){
	MapLayer *layer = new MapLayer(&ts);
	vector<int> data;
	int lx=0, ly=0;
	lx = d["x"];
	ly = d["y"];
	layer->setName(d["name"]);

	if(d["encoding"] == "base64")
	  throw std::runtime_error("Cannot accept the map file. Base64 decoding has not yet been implemented!");

	try{

	  for(json t : d["data"]){
	    data.push_back(t);
	  }
	  //      cout<<d["data"]<<endl;
	}catch(exception &ex){
	  cout<<"Exception occured! "<<ex.what()<<endl;
	}

	try{

	    lw = d["width"];
        lh = d["height"];
	}catch(exception &ex){
	  cout<<"Exception occured! "<<ex.what()<<endl;
	}

	if(d.size() > 0){
	  layer->setPosition(lx, ly);
	  layer->setData(data);
	  layer->setSize(lw, lh);
	  layer->setTileSize(tw, th);
	  cout << "load completed \n";
	  cout << "====Layer data==== \n" << layer->toString();
	}
	ts.addLayer(*layer);
      }
    //if(d["data"]) data = d["data"];
    //cout<<d["data"] <<endl;

    //layer->setData(d);
    //layer->setSize(lw, lh);
    //  layer->setTileSize(tw, th);

  }

  //  cout << jsonData["layers"] <<endl;
  //  exit(-1);
}
