#include "SpriteLoader.hpp"
#include <fstream>


SpriteLoader::  SpriteLoader(IVideoDriver* driver){
    this->driver = driver;
}

IrrSprite* SpriteLoader::loadSprite(const wchar_t* file,
				    std::vector<GameRect> frameClips){
    ITexture *tex = driver->getTexture("data/spriteSheet.png");
    IrrSprite* spr= new IrrSprite(tex);
    for(GameRect r : frameClips){
      spr->addFrame(core::rect<s32>(r.x1, r.y1, r.x2, r.y2));
    }
    return spr;
}

IrrSprite* SpriteLoader::loadSpriteFromJSON(json j,
					      std::string directory){
  {
    IrrSprite *spr = NULL;
    
    struct spriteFormat{
      int offsetx;
      int offsety;
      int count;
      int cols;
      int width;
      int height;
      std::string filename;
    };

    //json o = j["offset"];
    try{
      spriteFormat data{
	j["offset"]["x"],
	  j["offset"]["y"],
	  j["count"],
	  j["cols"],
	  j["width"],	 
	  j["height"],
	  j["file"]
	  };

      std::vector<GameRect> frameClips;      
      for(int n = 0; n < data.count-1; n++){ //count in the json file must start from 1
	int x, y;
	x = n % data.cols;
	y = n / data.cols;
	x *= data.width;
	y *= data.height;
	x += data.offsetx;
	y += data.offsety;
	frameClips.push_back(GameRect{x,y, x+data.width, y+data.height});
      }
      std::string fpath = directory + data.filename;
      std::wstring fname_w(fpath.begin(), fpath.end());
      spr = loadSprite(fname_w.c_str(), frameClips);
    }catch(std::exception &ex){
      std::cout << "Sorry. Cannot parse JSON sprite file. Exception: " << ex.what() << std::endl;
    }
    
    return spr;

  }
}

IrrSprite* SpriteLoader::loadSpriteFromString(std::string json_data,
					      std::string directory){
    json j = json::parse(json_data.c_str());
    return loadSpriteFromJSON(j, directory);
}

IrrSprite* SpriteLoader::loadSpriteFromFile(std::string filename,
					    std::string directory){
  ifstream i(directory+"/"+filename);
  if(!i.is_open())
    throw std::runtime_error("File "+ filename+" not found in directory "+directory+".");

  json j;
  j<<i;
  return loadSpriteFromJSON(j, directory);
}
