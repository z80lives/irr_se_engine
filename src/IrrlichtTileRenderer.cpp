#include "IrrlichtTileRenderer.h"

#include <iostream>
using namespace std;


IrrTileRenderer::IrrTileRenderer(IVideoDriver* drv){
  driver = drv;
}

void IrrTileRenderer::setTileClip(int tile_id,
				  int x1, int y1,
				  int x2, int y2)
{
  tile_list[tile_id].x1 = x1;
  tile_list[tile_id].y1 = y1;
  tile_list[tile_id].x2 = x2;
  tile_list[tile_id].y2 = y2;
}

void IrrTileRenderer::setTileSize(int tile_id, int w, int h){
  tile_list[tile_id].w = w;
  tile_list[tile_id].h = h;
}

void IrrTileRenderer::setElevation(int tile_id, int el){
  tile_list[tile_id].elevation = el;
}



/** 
 * Draws the given tile layer and all the objects on the tilelayer
 *
 **/
void IrrTileRenderer::drawLayer(MapLayer* layer){
  int xoff = layer->getXoffset(), yoff = layer->getYoffset();

  int width = layer->getTileWidth();
  int height = layer->getTileHeight();

  int v_tiles = layer->getWidth();
  int h_tiles = layer->getHeight();

  for(int y=0; y < v_tiles; y++){
    for(int x=0; x <  h_tiles; x++){

      int tileId = layer->getTileId(x,y);
      //tileId = 1;//debug
      if(tileId == 0)
        continue;

      Tile tileinfo = tile_list[tileId];

      //apply new tile dimension if it has been overriden
      /*if(tileinfo.w != 0)  //use map tile dimensions
	width = tileinfo.w;
      if(tileinfo.h != 0)
	height = tileinfo.h ;*/

      int elevation = tileinfo.elevation;

      core::position2d<s32> pos( ( x * width / 2 - y * width / 2) + xoff,
				 (x * height / 2 + y * height / 2) + yoff
				 - elevation
				 );


      core::rect<s32> dest_rect(tileinfo.x1,
				tileinfo.y1,
				tileinfo.x2,
				tileinfo.y2);

      driver->draw2DImage(getTexture(tileId),
			  pos,
			  dest_rect,
			  0,
			  video::SColor(255, 255, 255, 255),
			  true
			  );

      //@todo draw all objects on this tile
      // objects need to be referred to in a in priority queue, indexed  by beforehand according to the objects current recalculated tile index
    }
  }

}


void IrrTileRenderer::loadTilesets(const TilingSystem& tilsys, string dir)
{
    for(int i =0; i < tilsys.countTilesets(); i++){
        loadTileset(tilsys.getTileset(i), dir);
    }
}



void IrrTileRenderer::addTexture(int id, ITexture* tex){
  texture_map[id] = tex;
}


void IrrTileRenderer::loadTileset(const Tileset  &t, string dir){
  video::ITexture* tile_sheet = driver->getTexture((dir+t.filename).c_str());

  //driver->makeColorKeyTexture(wall_texture, core::position2d<s32>(0,0));
  //driver->makeColorKeyTexture(tile_sheet, core::position2d<s32>(0,0));

for(int i=0; i< t.tilecount; i++){
  int xclip = (i % t.columns) * 64 ;
  int yclip = (i / (t.width / t.tilewidth) ) * t.tilewidth;
  addTexture(i, tile_sheet);
  setTileSize(i, t.tilewidth, t.tileheight);
  setTileClip(i, xclip, yclip, xclip+t.tilewidth, yclip+ t.tileheight);
}

  //addTexture(22, tile_sheet);
  //setTileSize(22, 64, 32);
  //setTileClip(22, 64*2, 64*5, 64* 2 + 64, 64*5 + 64);


  //addTexture(0, 0,0, 32, 64);
}


//for test purpose
/*void IrrTileRenderer::drawTexture(int tile_index, int x, int y, core::rect<s32> clip)
{
    ITexture* tex = getTexture(tile_index);
    Tiles tileinfo = tile_list[tile_index];

    core::position2d<s32> pos(x,y);


      core::rect<s32> dest_rect(tileinfo.x1,
                                 tileinfo.y1,
                                 tileinfo.x2,
                                 tileinfo.y2);

      driver->draw2DImage(tex,
                          pos,
                          dest_rect,
                          0,
                          video::SColor(255, 255, 255, 255),
                          true
                    );
}*/



void IrrTileRenderer::drawTile(int tile_index, int x, int y){
    ITexture* tex = getTexture(tile_index);
    Tiles tileinfo = tile_list[tile_index];

    core::position2d<s32> pos(x,y);


      core::rect<s32> dest_rect(tileinfo.x1,
                                 tileinfo.y1,
                                 tileinfo.x2,
                                 tileinfo.y2);

      driver->draw2DImage(tex,
                          pos,
                          dest_rect,
                          0,
                          video::SColor(255, 255, 255, 255),
                          true
                    );
}
