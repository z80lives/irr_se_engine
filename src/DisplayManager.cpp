#include "DisplayManager.hpp"
#define UPDATE_COUNT 20
DisplayManager::DisplayManager(IVideoDriver* driver){
  this->driver = driver;
}

int DisplayManager::anim_count = 0;

void DisplayManager::draw(){       
  /*for(IrrSprite spr : spritelist){
      spr.draw(driver, 0, 30, 30);      
      }*/
  
  for(Actor *actor : actorlist){
    Vector2i p = actor->getPosition();
    actor->getSprite()->draw(driver, actor->getCurrentFrame(),
			     p.x, p.y);

    if(anim_count == UPDATE_COUNT){
      actor->nextFrame();
    }
    
    anim_count ++;
    if(anim_count > UPDATE_COUNT){
      anim_count = 0;
    }
    
  }


}

void DisplayManager::addActor(Actor* actor){
    actorlist.push_back(actor);
}
