#include "GameManager.hpp"
#include "../inc/InputEventReceiver.h"



GameManager::GameManager(){
  screenSize = dimension2d<s32>(800,600);
  device = createDevice(EDT_OPENGL,
			core::dimension2d<u32>(800, 600),
			16,
			false, false, true, 0);

  //device->setEventReceiver(eventhandler);
  
  driver = device->getVideoDriver();
  smgr = device->getSceneManager();
  guienv = device->getGUIEnvironment();
  scriptSystem = new ScriptingSystem();

  debugConsole = new IrrConsoleInput(guienv,
				     0,
				     core::rect<s32>(0,0,screenSize.Width,
						     screenSize.Height/2-20),
				     scriptSystem
				     );
  eventhandler = new InputEventReceiver(debugConsole);
  device->setEventReceiver(eventhandler);
  
  spriteLoader = new SpriteLoader(driver);
  displayManager = new DisplayManager(driver);
}

void GameManager::initialize(){
  device->setWindowCaption(L"Assignment 2 Prototype");
 
}

void GameManager::run(){
  while(device->run()){
    driver->beginScene(true, true, video::SColor(255, 120, 102, 136));
    displayManager->draw();
    
    smgr->drawAll();
    
    guienv->drawAll();
    
    driver->endScene();
  }
}

SpriteLoader* GameManager::getSpriteLoader(){
  return spriteLoader;
}

GameManager::~GameManager(){
  device->drop();
  if(debugConsole != NULL)
    debugConsole->drop();
  delete  eventhandler;
}
