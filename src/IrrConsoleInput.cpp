/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   IrrConsoleInput.cpp
 * Author: shath
 * 
 * Created on 20 November 2017, 05:01
 */

#include "IrrConsoleInput.h"

IrrConsoleInput* IrrConsoleInput::singleton = NULL;

 IrrConsoleInput::IrrConsoleInput(IGUIEnvironment* gui, IGUIElement* parent,
         core::rect<s32> rectangle, ScriptingSystem* scriptSys)
 :  IGUIElement(EGUIET_EDIT_BOX, gui, parent, -1, rectangle)
 {
     if(singleton == NULL)
         singleton = this;
     
     scriptingSystem = scriptSys;
     
     visible = true;
     this->gui = gui;
     
     int kheight = 40;    
     this->font = gui->getBuiltInFont();
     font->setKerningHeight(kheight);
     //gui->getBuiltInFont()->setKerningWidth(40);
     
     gui->getSkin()->setFont(gui->getFont("data/fonthaettenschweiler.bmp"));
    
     
     context = gui->addTab(rectangle, parent);   
     
     
     rect<s32> inpArea(
             rectangle.UpperLeftCorner.X,
             rectangle.LowerRightCorner.Y - kheight - 1,
             rectangle.LowerRightCorner.X,
             rectangle.LowerRightCorner.Y
      );
         
     editbox = gui->addEditBox(L"", inpArea, true, context, +1);
     editbox->addChild(this);     
     //editbox->
     gui->setFocus(editbox);
     editbox->setEnabled(true);
     
     //Create console output are
     rect<s32> outArea(
             rectangle.UpperLeftCorner.X,
             rectangle.UpperLeftCorner.Y,
             rectangle.LowerRightCorner.X,
             rectangle.LowerRightCorner.Y - kheight - 1
      );
     
     console_out = gui->addListBox(outArea, context); 
     console_out->setAutoScrollEnabled(true);
     console_out->setDrawBackground(true);
     
     if(scriptSys != NULL){
         scriptSys->setExceptionHandler(handleExceptions);
         scriptSys->setPrintHandler(handleLog);
         scriptSys->setClearScreen(clearScreen);
     }
     
     setVisible(false);
}
 void IrrConsoleInput::clearScreen() {
     singleton->clear();
}


 void IrrConsoleInput::handleLog(std::string msg) {
     wstring text(msg.begin(), msg.end());
     stringw t(text.c_str());
     singleton->addText(t);
}

 
 void IrrConsoleInput::handleExceptions(int i, std::string msg, std::exception_ptr ex) {     
     wstring text(msg.begin(), msg.end());
     stringw t(text.c_str());
     singleton->addText(t);
}


 
 
void IrrConsoleInput::clear() {
    console_out->clear();
}


bool IrrConsoleInput::handleEvents(const SEvent& event) {
    if(!visible)
        return true;
    if(event.KeyInput.Key == 13){                    
            //retrieve and clear input box
            stringw inputText = editbox->getText();
            
            editbox->setText(L"");
            
            if(inputText.size() > 0){
                this->addText(inputText);
                std::wcout<< "Input was: "<< inputText.c_str()<<endl;   
            }
            
            if(scriptingSystem != NULL){                
                scriptingSystem->eval(inputText.c_str());                
                //std::string outText = scriptingSystem->getResult();                
            }
    }if(event.KeyInput.Key == KEY_ESCAPE){
        setVisible(false);
    }
    return true;
}


void IrrConsoleInput::draw() {
    IGUIElement::draw();
    if(isVisible()){
        //gui->getVideoDriver()->draw2DRectangle(video::SColor(255, 0, 128, 255), 
        //    rect<s32>(10, 140, 600, 210), NULL);
    }
}

void IrrConsoleInput::addText(core::stringw str) {
    console_out->addItem(str.c_str());   
    console_out->setSelected(console_out->getItemCount()-1);
    
    if(!console_out->isAutoScrollEnabled())
        console_out->setAutoScrollEnabled(true);
    
}


bool IrrConsoleInput::OnEvent(const SEvent& event) {
    IGUIElement::OnEvent(event);
      if(event.EventType == EET_KEY_INPUT_EVENT){    
          cout<<event.KeyInput.Char;
        if(event.KeyInput.Key == 13){               
            //console_out->addItem(editbox->getText());
            //cout << "Edit box event called"<<endl;
            //editbox->setText(L"das");             
            //font->draw()            
            //gui->addStaticText(L"DSASD", rect<s32>(0,40, 300, 300));
        }         
    }
    return false;
}

IGUIEditBox* IrrConsoleInput::getEditbox() const {
    return editbox;
}


void IrrConsoleInput::addChild(IGUIElement* child) {
    IGUIElement::addChild(child);
}

bool IrrConsoleInput::isVisible() const {
    return visible && IGUIElement::isVisible();
}

void IrrConsoleInput::setVisible(bool visible) {
    this->visible =  visible;
    context->setVisible(visible);
    console_out->clear();
    editbox->setEnabled(visible);
    IGUIElement::setVisible(visible);    
}
