/*
 * Here comes the text of your license
 * Each line should be prefixed with  * 
 */

/* 
 * File:   ScriptingSystem.cpp
 * Author: shath
 * 
 * Created on 20 November 2017, 20:27
 */

#include "ScriptingSystem.h"

#include <sstream>

//for debug purpose
#include <iostream>
#include <stdexcept>
using namespace std;

//static void exhandler(int,std::string, std::exception_ptr){
    
//}

void ScriptingSystem::setExceptionHandler(std::function<void(int,std::string,std::exception_ptr)> fn) {
    state.HandleExceptionsWith(fn);    
}

void ScriptingSystem::setPrintHandler(std::function<void(std::string)> fn) {
    state["log"]=  fn;
}

void ScriptingSystem::setClearScreen(std::function<void()> fn) {
    state["clear"]=  fn;
}


ScriptingSystem::ScriptingSystem() {
}

string ScriptingSystem::typeCast(const wchar_t* str) {
    wstring ws(str);
    string result(ws.begin(), ws.end());
    return result;
}


void ScriptingSystem::eval(string expression) {
    if(expression == "")
        return;
    //freopen("output.txt", "w", stdout);
    //cout << "Evaluating \""<<expression<<"\"."<<endl;
    state(expression.c_str());
    
    //freopen(stdout, "w", stdout);
}

void ScriptingSystem::eval(const wchar_t* t) {
    eval(typeCast(t));
}

string ScriptingSystem::getResult() const {
    std::stringstream sb("");
    //state.HandleExceptionsPrintingToStdOut()
    //sel::_print()
    return sb.str();
}
