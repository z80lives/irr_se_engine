#include "../inc/InputEventReceiver.h"
#include <iostream>
using namespace std;

InputEventReceiver::InputEventReceiver(IrrConsoleInput* dbgConsole)
{
  //initialize arrKey[]
    for(u32 i=0; i<KEY_KEY_CODES_COUNT; i++)
        arrKey[i] = false;
    debugConsole = dbgConsole;
}

InputEventReceiver::~InputEventReceiver()
{

}

bool InputEventReceiver::OnEvent(const SEvent& event)
{


  if(event.EventType == EET_KEY_INPUT_EVENT){
    arrKey[event.KeyInput.Key] = event.KeyInput.PressedDown;
    //Debug console events
    if(debugConsole->isVisible())
      debugConsole->handleEvents(event);    
    if(event.KeyInput.Key == KEY_OEM_3){
      if(! debugConsole->isVisible()){
	debugConsole->setVisible(true);
      }
    }else if(event.KeyInput.Key == KEY_ESCAPE){
      if(!debugConsole->isVisible())
	debugConsole->setVisible(false);
    }      
  }    



  // Remember the mouse state
  if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
    {
      switch(event.MouseInput.Event)
	{
	case EMIE_LMOUSE_PRESSED_DOWN:
	  MouseState.leftButtonDown = true;
	  break;

	case EMIE_LMOUSE_LEFT_UP:
	  MouseState.leftButtonDown = false;
	  break;

	case EMIE_MOUSE_MOVED:
	  MouseState.position.X = event.MouseInput.X;
	  MouseState.position.Y = event.MouseInput.Y;
	  break;

	default:
	  // We won't use the wheel
	  break;
	}
    }

  // The state of each connected joystick is sent to us
  // once every run() of the Irrlicht device.  Store the
  // state of the first joystick, ignoring other joysticks.
  // This is currently only supported on Windows and Linux.
  if (event.EventType == irr::EET_JOYSTICK_INPUT_EVENT
      && event.JoystickEvent.Joystick == 0)
    {
      joystickState = event.JoystickEvent;
    }


  return false;

}

bool InputEventReceiver::IsKeyDown(EKEY_CODE keyCode) const
{
    return arrKey[keyCode];
}

bool InputEventReceiver::keyArrayIsEmpty()
{
  return true;
}

bool InputEventReceiver::moveActors()
{

  return true;
}

bool InputEventReceiver::enableJoystick(IrrlichtDevice* dev)
{
  return true;
}
