/*#include <iostream>
#include <irrlicht.h>
#include <IImageLoader.h>

//#include "inc/InputEventReceiver.h"


#include "TilingSystem.h"
#include "IrrlichtTileRenderer.h"

#include "LevelReader.h"

#include "IrrSprite.h"
#include "Actor.h"

#include "IrrTestNode.h"
*/

#include "GameManager.hpp"
//#define ANIM_DELAY 10


/**
 * Code to slow down irrlicht
 **/
/*struct IrrlichtDelayFPS {
  int maxFPS;
  int lastFPS;
  int yieldCounter;
  IrrlichtDevice *device;
  IrrlichtDelayFPS() { maxFPS = 50;  yieldCounter = 0;  device = 0; }
  void delay() {
    video::IVideoDriver* driver = device->getVideoDriver();
    int lastFPS = driver->getFPS();
    for( int i = 0 ; i < yieldCounter ; i++ ) {
      device->yield();
    }
    if ( lastFPS >maxFPS ) { yieldCounter++ ; }
    else {
      if (yieldCounter > 0 ) { --yieldCounter; }
      else { yieldCounter = 0 ; }
    }
  }
  };*/

#include "Actor.h"
#include <vector>

int main(int argc, char**argv){
  GameManager gm;

  gm.initialize();

  /*ITexture *tex = driver->getTexture("data/spriteSheet.png");
  
  IrrSprite spr(tex);
  spr.addFrame(core::rect<s32>(0,0, 50, 46));
  spr.addFrame(core::rect<s32>(50,0, 100, 46));
  */

  //std::string spritedata = "{\"offset\": {\"x\":0, \"y\": 0}, \"count\": 8, \"width\": 50, \"height\": 46, \"cols\": 2, \"file\": \"spriteSheet.png\"}";
  std::vector<GameRect> framevect = {{0,0, 50, 46}, {50, 0, 100, 46}};
  //  IrrSprite* spr = gm.getSpriteLoader()->loadSprite(L"data/spriteSheet.png",								    framevect);

  IrrSprite* spr = gm.getSpriteLoader()->loadSpriteFromFile("player.json",
  							      "data");

  //  gm.getDisplayManager()->addSprite(spr);
  
  Actor *bird = new Actor();
  bird->setSprite(spr);
  gm.getDisplayManager()->addActor(bird);

  bird->setAnimRange(2, 3);  
    
  gm.run();
  /*    InputEventReciever input_handler;
    IrrlichtDevice *device = createDevice(EDT_OPENGL,
        core::dimension2d<u32>(800, 600), 16, false, false, true, 0);

    device->setEventReceiver(&input_handler);
    device->setWindowCaption(L"Assignment 2 Prototype");

    IVideoDriver* driver = device->getVideoDriver();
    ISceneManager* smgr = device->getSceneManager();
    IGUIEnvironment* guienv = device->getGUIEnvironment();

    //setup 3d camera
    smgr->addCameraSceneNode(0, core::vector3df(0,-200,0), core::vector3df(0,0,0));

    TilingSystem tilesys;
    LevelReader lvl1("data/level/", "level_0.room.json");

    lvl1.loadLevel(tilesys);

    IrrTileRenderer maprenderer(driver);
    maprenderer.loadTilesets(tilesys, "data/level/");

    core::rect<s32> chick(300, 20, 350, 40);


    //create font
    gui::IGUIFont* font = device->getGUIEnvironment()->getBuiltInFont();

    
    //load player sprites
    ITexture *tex = driver->getTexture("data/spriteSheet.png");

    IrrSprite spr(tex);
    spr.addFrame(core::rect<s32>(0,0, 50, 46));
    spr.addFrame(core::rect<s32>(50,0, 100, 46));
    //IrrSprite frame2(tex, core::rect<s32>(50,0, 100, 46));

    Actor test_act;
    test_act.setSprite(&spr);




    ISceneNode* cube = smgr->addCubeSceneNode();
    cube->setPosition(vector3df(0,-100, 0));
    smgr->setAmbientLight(video::SColorf(0.3,0.3,0.3,1));
    ITexture *mtex = driver->getTexture("data/walls.bmp");
    SMaterial* mat = new SMaterial();
    mat->setTexture(0, mtex);
    driver->setMaterial(*mat);
    driver->setTransform(video::ETS_WORLD, core::IdentityMatrix);

    ILightSceneNode* light1 = smgr->addLightSceneNode( 0,
						       core::vector3df(0, -150, 0),
						       video::SColorf(0.3f,1.0f,0.3f),
						       1.0f, 1 );
    light1->setRadius(100);
    //light1->enableCastShadow();
    light1->setVisible(true);
    
    cube->setMaterialFlag(EMF_LIGHTING, true);
    
    //cube->getMaterial(0).SpecularColor.set(255,255,0,255);
    //cube->getMaterial(0).AmbientColor.set(128,0,255,255);
    //cube->getMaterial(0).DiffuseColor.set(128,255,0,255);
    //cube->getMaterial(0).EmissiveColor.set(0,0,0,0);

    scene::ISceneNodeAnimator* anim = smgr->createRotationAnimator(core::vector3df(0.8f, 0, 0.8f));
    cube->addAnimator(anim);
    anim->drop();


    ICursorControl *cursor = device->getCursorControl();

    //smgr->addParticleSystemSceneNode();

    double mapx = 100, mapy = 30;

    std::cout << "iostream is working"<<std::endl;

    int n=0;
    u32 frames;
    u32 anim_time=0;
    while(device->run() && driver ){
      core::stringw out_string("log: ");
	
        if(input_handler.IsKeyDown(irr::KEY_ESCAPE))
            break;
        if(input_handler.IsKeyDown(irr::KEY_KEY_W)){
            mapy += 3;
        }else if(input_handler.IsKeyDown(irr::KEY_KEY_S)){
            mapy -= 3;
        }

        if(input_handler.IsKeyDown(irr::KEY_KEY_A)){
            mapx += 3;
        }else if(input_handler.IsKeyDown(irr::KEY_KEY_D)){
            mapx -= 3;
        }

	cube->setPosition(vector3df(-mapy -200,200, mapx));


        if(device->isWindowActive()){


            driver->beginScene(true, true, video::SColor(255, 120, 102, 136));
            guienv->drawAll();


        int obj_layer = 0;
        unsigned int i = 2;
	    //Draw all tile layers
	    for(unsigned int i=0; i < tilesys.getLayers().size(); i++){
	      MapLayer *layer = tilesys.getLayer(i);
	      
	      if(layer != NULL ){
		layer->setPosition(mapx, mapy);
		maprenderer.drawLayer(layer);   //@todo draw all objects on this tile
            }

	      if(i == obj_layer)
		smgr->drawAll();

        }

	    core::position2d<s32> m = device->getCursorControl()->getPosition();
 	 
	    //Draw 3d line
	    {

	      int tw = 64;
	      int th = 32;
	      
	      tilesys.setOffset(32, 32);
	      SE_Vector2d topPixel = tilesys.calcTileScreenPos(0,
							       0);
	      SE_Vector2d tp = tilesys.calcScreenTilePos(m.X - mapx,
							 m.Y - mapy 
							 );	      
	      int x = tp.x, y = tp.y;

	      //append debug info
	      out_string += " tile_x:";
	      out_string += tp.x;
	      out_string += " tile_y:";
	      out_string += tp.y;
	      out_string += " layer0_tileid: ";
	      out_string += tilesys.getLayer(0)->getTileId(x,y);
	      out_string += " layer1_tileid: ";
	      out_string += tilesys.getLayer(1)->getTileId(x,y);

	      
	      SE_Vector2d tilepos = tilesys.calcTileScreenPos(x,
							      y);
	      x = tilepos.x + mapx ;
	      y = tilepos.y + mapy ;
	      
	      driver->setTransform(video::ETS_WORLD, core::IdentityMatrix);

	      tw  = 32;
	      th  = 16;
	      
	    //draw grid
	    driver->draw2DLine(position2d<s32>(x,
					       y),
			       position2d<s32>(x+tw,
					       y+th),
	    		     SColor(255,255,0,0));
	    driver->draw2DLine(position2d<s32>(x,
					       y),
			       position2d<s32>(x-tw,
					       y+th),
	    		     SColor(255,255,0,0));
	    driver->draw2DLine(position2d<s32>(x+tw,
					       y+th),
			       position2d<s32>(x,
					       y+th*2),
			       SColor(255,255,0,0));
	    driver->draw2DLine(position2d<s32>(x-tw,
					       y+th),
			       position2d<s32>(x,
					       y+th*2),
			       SColor(255,255,0,0));
	    
	    }
	    
	    //draw cursor box
         driver->draw2DRectangle(video::SColor(100,255,255,255),
                            core::rect<s32>(m.X-20, m.Y-20, m.X+20, m.Y+20));

         /*core::position2d<s32> m = device->getCursorControl()->getPosition();
                                driver->draw2DRectangle(video::SColor(100,255,255,255),
                            core::rect<s32>(m.X-20, m.Y-20, m.X+20, m.Y+20));*/


          /*driver->draw3DBox(core::aabbox3d<f32>(vector3d<f32>(0, -300, 0)),
                            video::SColor(255, 128, 0, 255));*/
/*
	 //draw string
	  font->draw(out_string,
                    core::rect<s32>(0,0,300,50),
                    video::SColor(255,255,255,255));


         if(anim_time++ == ANIM_DELAY){
            //animate
            n++;
            anim_time = 0;
         }



            n= (n>1)? 0 : n;
            spr.draw(driver, n, mapx + 200, mapy + 200);

            driver->endScene();

            if (++frames==100)
            {
                core::stringw str = L"Assignment prototype [";
                str += driver->getName();
                str += L"] FPS: ";
                str += (s32)driver->getFPS();

                device->setWindowCaption(str.c_str());
                frames=0;
            }
        }
    }

//    reciever->drop();
device->drop();
*/
    return 0;
}

