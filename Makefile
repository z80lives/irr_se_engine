MAIN_FILE = main.cpp
MODE = debug

SOURCES = $(filter-out src/$(MAIN_FILE), $(wildcard src/*.cpp))
INCLUDE_DIR = inc 
OBJ_DIR = obj
OBJS := $(SOURCES:src/%.cpp=$(OBJ_DIR)/%.o)
CC = g++

LIBS = xxf86vm gl x11 xcursor xext lua5.2

DEBUG_FLAGS = -g -Wall
CFLAGS = -std=c++11 -Wall -I$(INCLUDE_DIR) -Iext/include -I/home/shath/Public/build/irrlicht-1.8.4/include/ `pkg-config --cflags $(LIBS)` $(DEBUG_FLAGS)

OUTPUT_DIR = bin/linux


LFLAGS = -L/home/shath/Public/build/irrlicht-1.8.4/lib/Linux  `pkg-config --libs $(LIBS)` -lIrrlicht
EXEC_NAME = main

#This is the target that compiles our executable

$(OBJ_DIR)/%.o: src/%.cpp
	@echo Compiling $@
	@$(CC) -c $(CFLAGS) $< -o $@

all : $(OBJS)
	@echo Compiling and linking main file
	@$(CC) $(MAIN_FILE) $(OBJS) $(CFLAGS) $(LFLAGS) -o $(OUTPUT_DIR)/$(EXEC_NAME) $(DEBUG_FLAGS)
	@echo Done


clean:
	@echo Deleting object files
	@rm obj/*.o
