#ifndef INPUTEVENTRECEIVER_H
#define INPUTEVENTRECEIVER_H

#include "IrrConsoleInput.h"

#include <irrlicht.h>
using namespace irr;

/*#ifndef ACTOR_H
    #include "Actor.h"
#endif // ACTOR_H

#ifndef CAMERA_H
    #include "Camera.h"
#endif // CAMERA_H
*/

#define __DEBUG_PRINT_CONSOLE__ //Enable debug mode

#define JOYPAD_AXIS_RANGE 32767.f

class InputEventReceiver : public IEventReceiver
{

    public:
        enum DEFAULT_MAPS{
            WASD,
            ARROW_SPACE_CTRL,
            INP_COUNT
        };
        enum Control_Schemes{
            CARTESIAN_MOVE,
            TANK_MOVE,
            CTRL_SCHEME_COUNT
        };
        enum InputKeys{
            UP_BTN,
            DOWN_BTN,
            LEFT_BTN,
            RIGHT_BTN,
            BTN_A,
            BTN_B,
            BTN_X,
            BTN_Y,
            BTN_R1,
            BTN_R2,
            BTN_L1,
            BTN_L2,
            BTN_START,
            BTN_SELECT,
            LEFT_STICK,
            LEFT_STICK_X,
            LEFT_STICK_Y,
            RIGHT_STICK,
            RIGHT_STICK_X,
            RIGHT_STICK_Y,
            BTN_COUNT
        };
        // A structure to hold mouse state
        struct StructMouseState{
            core::position2di position;
            bool leftButtonDown;
            StructMouseState() : leftButtonDown(false) {}
        }MouseState;

        InputEventReceiver(IrrConsoleInput* dbgConsole);
        virtual ~InputEventReceiver();
        virtual bool OnEvent(const SEvent& event);
        virtual bool IsKeyDown(EKEY_CODE keyCode) const;
        bool keyArrayIsEmpty();

        /** Attach actor to pre-defined input scheme
         *
         *
         */
        //bool attachActor(Actor*, Camera*, unsigned int, unsigned int);

        /** Set frameDelta
         * \param val New value to set
         */
        void setFrameDelta(unsigned int val) { frameDelta = val; }


        /** Moves all the actors assigned to input
         */
        bool moveActors();

        /**
         * Get Joystick state
         */
        const SEvent::SJoystickEvent & GetJoystickState(void) const
        {
            return joystickState;
        }

        /**
         * Get mouse state
         */
        const StructMouseState & GetMouseState(void) const
        {
            return MouseState;
        }

        /**
        * Enables joystick
        * \return True on success
        */
        bool enableJoystick(IrrlichtDevice* dev);

    protected:

    private:
        SEvent::SJoystickEvent joystickState;
        core::array<SJoystickInfo> joystickInfo;
  IrrConsoleInput* debugConsole;
        bool arrKey[KEY_KEY_CODES_COUNT];
//        Actor* plyr = NULL;
//        Camera* plyrCam = NULL;
        f32 frameDelta; //Required by animation routine
};

#endif // INPUTEVENTRECIEVER_H
