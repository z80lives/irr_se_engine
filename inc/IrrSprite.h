#ifndef IRRSPRITE_H
#define IRRSPRITE_H

#include <vector>
#include "core.hpp"
#include <irrlicht.h>

using namespace std;
using namespace irr;
using namespace core;
using namespace video;

class IrrSprite
{
    public:
        IrrSprite(ITexture* tex);

        void draw(video::IVideoDriver* driver, int frame, int x, int y);
        void addFrame(core::rect<s32> clipping);
	int countFrames() const{ return frames.size(); }
    protected:
        //core::rect<s32> clip;
    private:
        vector<core::rect<s32> > frames;
        //int current_frame;
        ITexture *texture;
};

#endif // IRRSPRITE_H
