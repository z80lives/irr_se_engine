#ifndef ACTOR_H
#define ACTOR_H

#include "IrrSprite.h"

class Actor
{
    public:
    enum ANIM_STATE{
       ANIM_STOP,
       ANIM_LOOP
    };
        Actor();
       ~Actor();
        void setSprite(IrrSprite* spr){
	  sprite = spr;
	  lastFrame = spr->countFrames()-1;
	}
        IrrSprite* getSprite() const{return sprite;}
	Vector2i getPosition(){
	  return position;
	}
	void setPosition(int x, int y){
	  position.x = x;
	  position.y = y;
	}

	int getCurrentFrame(){
	  return currentFrame;
	}
	void setFrame(int n ){
	  currentFrame = n;
	}

	void setAnimRange(int start, int end){
	  currentFrame = start;
	  firstFrame = start;
	  lastFrame = end;
	}

	void nextFrame();
    protected:

    private:	
	Vector2i position;
        int x, y;
        IrrSprite* sprite;
	int currentFrame;
	int firstFrame;
	int lastFrame;
};

#endif // ACTOR_H
