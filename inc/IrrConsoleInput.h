/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   IrrConsoleInput.h
 * Author: shath
 *
 * Created on 20 November 2017, 05:01
 */

#ifndef IRRCONSOLEINPUT_H
#define IRRCONSOLEINPUT_H

#include <iostream>
using namespace std;

#include <irrlicht.h>

#include "ScriptingSystem.h"

#include <stdexcept>

using namespace irr;
//using namespace video;
using namespace gui;
using namespace core;


class IrrConsoleInput : public IGUIElement  {
    static IrrConsoleInput* singleton;
    IGUIEditBox* editbox;
    IGUIEnvironment* gui;
    IGUIFont* font;
    IGUIElement* context;
    IGUIListBox* console_out;
    ScriptingSystem* scriptingSystem;
    
    stringw text;  
    
    bool visible;
protected:
    static void handleExceptions(int,std::string,std::exception_ptr);  
    static void handleLog(std::string);
    static void clearScreen();
public:
  
    IrrConsoleInput(IGUIEnvironment* gui, IGUIElement* parent, core::rect<s32> rectangle,
            ScriptingSystem* scripting_sytem = NULL
            
            ) ;

    void draw() override;
    bool OnEvent(const SEvent& event) override;
    void addChild(IGUIElement* child) override;
    void addText(core::stringw str);
    
    bool handleEvents(const SEvent& event);
    void clear();
   
    bool isVisible() const override;

    void setVisible(bool visible) override;

    
    IGUIEditBox* getEditbox() const;

};

#endif /* IRRCONSOLEINPUT_H */
