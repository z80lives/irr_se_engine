#ifndef IRRDEBUGCONSOLE_H
#define IRRDEBUGCONSOLE_H

#include <vector>
#include <irrlicht.h>

using namespace irr;
using namespace video;
using namespace gui;


class IrrDebugConsole{
 public:
  IrrDebugConsole(video::IrrlichtDevice *device);
  void render();
  
private:
  IGUIEnvironment* guiEnv;
  IrrlichtDevice* device;
}

#endif
