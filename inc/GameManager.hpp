#ifndef GAMEMANAGER_HPP
#define GAMEMANAGER_HPP

#include <irrlicht.h>

#include "core.hpp"
#include "SpriteLoader.hpp"
#include "DisplayManager.hpp"

#include "ScriptingSystem.h"

//the debug console
#include "IrrConsoleInput.h"



using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;


class GameManager{
  IrrlichtDevice* device;
  video::IVideoDriver* driver;
  scene::ISceneManager* smgr;
  gui::IGUIEnvironment* guienv;
  IEventReceiver* eventhandler;
  SpriteLoader* spriteLoader;
  DisplayManager* displayManager;
  IrrConsoleInput* debugConsole;
  ScriptingSystem* scriptSystem;
  
  core::dimension2d<u32> screenSize;
public:
  GameManager();
  ~GameManager();
  void initialize();
  void run();
  SpriteLoader* getSpriteLoader();
  DisplayManager* getDisplayManager(){
    return displayManager;
  };

};

#endif
