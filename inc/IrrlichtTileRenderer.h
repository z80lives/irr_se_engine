#ifndef TILE_RENDERER_H
#define TILE_RENDERER_H

#include <irrlicht.h>
#include "TilingSystem.h"
#include <map>

using namespace irr;
using namespace video;



class IrrTileRenderer{
  IVideoDriver* driver;
  map<int, ITexture*> texture_map; // ;)
  map<int, Tile> tile_list;
protected:
  ITexture* getTexture(int id){
    return texture_map[id];
  }

public:
  IrrTileRenderer(IVideoDriver* drv);
  void draw();
  void addTexture(int gid, ITexture* tex);
  void addTerrain(int gid, int tindex[4]);
  void drawLayer(MapLayer* layer);
  void drawTile(int tile_index, int x, int y);

  //for debug purpose
  void drawTexture(int tile_index, int x, int y, core::rect<s32> s);

  void setTileClip(int tile_id,
		   int x1, int y1,
		   int x2, int y2);

  void setTileSize(int tile_id,
		   int w, int h);
  void setElevation(int tile_id, int el);
  void loadTileset(const Tileset &tilmap, string dir);
  void loadTilesets(const TilingSystem &tilsys, string dir);
  ITexture* fetchTexture(int gid){
    return texture_map[gid];
  }
};

#endif

