#ifndef IRRTESTNODE_H
#define IRRTESTNODE_H
#include <irrlicht.h>

using namespace irr;
using namespace scene;

class IrrTestNode : public scene::ISceneNode
{
    public:
        IrrTestNode(scene::ISceneNode* parent, scene::ISceneManager* mgr, s32 id);
        virtual ~IrrTestNode();
        virtual void OnRegisterSceneNode();
        virtual void render();

        virtual const core::aabbox3d<f32>& getBoundingBox() const
        {
            return Box;
        }

        virtual u32 getMaterialCount() const
        {
            return 1;
        }

        virtual video::SMaterial& getMaterial(u32 i)
        {
            return Material;
        }
    protected:

    private:
            core::aabbox3d<f32> Box;
            video::S3DVertex Vertices[4];
            video::SMaterial Material;
};

#endif // IRRTESTNODE_H
