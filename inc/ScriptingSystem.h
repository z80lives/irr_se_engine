/*
 * Here comes the text of your license
 * Each line should be prefixed with  * 
 */

/* 
 * File:   ScriptingSystem.h
 * Author: shath
 *
 * Created on 20 November 2017, 20:27
 */

#include <selene.h>

#ifndef SCRIPTINGSYSTEM_H
#define SCRIPTINGSYSTEM_H
#include <string>
#include <functional>

using namespace sel;
using namespace std;


class ScriptingSystem{
    sel::State state{true};
public:
    ScriptingSystem();
    void eval(std::string expr);
    void eval(const wchar_t* t);
    std::string getResult() const;
    std::string typeCast(const wchar_t*  str);
    void setPrintHandler(std::function<void(std::string)> fn) ;
    void setClearScreen(std::function<void()> fn) ;
    void setExceptionHandler(std::function<void(int,std::string,std::exception_ptr)> fn);
};

#endif /* SCRIPTINGSYSTEM_H */
