#ifndef DISPLAYMANAGER_HPP
#define DISPLAYMANAGER_HPP

#include "core.hpp"
#include "IrrSprite.h"
#include <irrlicht.h>
#include <vector>

#include "Actor.h"

using namespace  irr;
using namespace video;



/**
 * Handles the sprites and the drawings
 **/
class DisplayManager{
  IVideoDriver* driver;
  std::vector<IrrSprite> spritelist;
  std::vector<Actor*> actorlist;

  static int anim_count;
public:

  DisplayManager(IVideoDriver* driver);
  
  //draw all sprites
  void draw();

  void addActor(Actor* actor);
  
  //Todo replace with add actor
  void addSprite(IrrSprite* spr){
    spritelist.push_back(*spr);
  }
};



#endif
