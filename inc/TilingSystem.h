#ifndef TILINGSYSTEM_H
#define TILINGSYSTEM_H
#include <string>
#include <vector>
#include <map>

using namespace std;

class MapObject{
    int id;
    int x;
    int y;
    string name;
};

struct SE_Vector2d{
  int x;
  int y;
};

struct Tileset{
  string filename;
  int columns = 0;
  int width = 0;
  int height = 0;
  int margin;
  int spacing;
  int tilecount=0;
  int tileheight;
  int tilewidth;
  string transparent_color;
};


typedef struct Tiles{
  int id;
  int x1;
  int y1;
  int x2;
  int y2;
  int w=0;
  int h=0;
  int elevation = 0;
}Tile;

class TilingSystem;
class MapLayer{
    int height;
    int width;
    string name;
    string type;
    int x;
    int y;
    int tile_width, tile_height;
    vector<MapObject*> objects;
    vector<int> tile_map;
  TilingSystem *tile_sys;
public:
  MapLayer(TilingSystem* ts){ tile_sys = ts; }
  int getWidth() const{return width;}
  int getHeight() const{ return height; }
  int getTileId(int x, int y);
  int getXoffset()const{ return x;}
  int getYoffset()const{ return y;}
  int getTileWidth() const {return tile_width;}
  int getTileHeight() const {return tile_height;}

  void setName(string _name){ this->name = _name;}
  void setPosition(int x, int y){ this->x = x; this->y = y; }
  void setTileSize(int w, int h){tile_width = w; tile_height = h;}
  void setData(int data[]);
  void setData(vector<int> data);
  void setSize(int w, int h){ width = w; height = h; }
  vector<int> getData(){return tile_map; }
  string toString();
};


class TilingSystem
{
public:
  TilingSystem();
  void setMapSize(int w, int h);
  void setTileSize(int w, int h);
  void addLayer(MapLayer &layer){
    layer.setTileSize(tile_width, tile_height);
    layers.push_back(&layer);
  }
  MapLayer* getLayer(int i){
    if(!layers[i])
      return NULL;
    return layers[i];
  }
  vector<MapLayer*> getLayers() const{return layers; }
  Tileset getTileset (int tileset_id)const{ return tilesets[tileset_id];}
  void addTileset(Tileset &t){ tilesets.push_back(t); }
  int countTilesets() const{ return tilesets.size(); }

  SE_Vector2d calcTileScreenPos(int x, int y);
  SE_Vector2d calcScreenTilePos(int sx, int sy);

  void setOffset(int x, int y){ xoffset = x; yoffset = y; }
protected:

private:
  vector<MapLayer*> layers;
  vector<Tileset> tilesets;
  string draworder;
  int tile_width, tile_height;
  int xoffset, yoffset;
  int map_width, map_height;
};


#endif // TILINGSYSTEM_H
