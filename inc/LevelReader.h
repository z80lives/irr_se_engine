#ifndef LEVELREADER_H
#define LEVELREADER_H
#include <string>


#include "TilingSystem.h"
#include <json.hpp>

using json = nlohmann::json;

class LevelReader
{
    private:
        json jsonData;
        string dir;
    public:
        LevelReader(std::string dir, std::string filename);
	void loadLevel(TilingSystem &ts);
	void loadTileset(Tileset &ts, string filename);
};

#endif // LEVELREADER_H
