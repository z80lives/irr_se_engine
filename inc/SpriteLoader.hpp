#ifndef SPRITELOADER_HPP
#define SPRITELOADER_HPP

#include "core.hpp"
#include "IrrSprite.h"
#include <irrlicht.h>
#include <vector>

using namespace  irr;
using namespace video;

#include "json.hpp"

#include <string>

#include <stdexcept>

using json = nlohmann::json;

class SpriteLoader{
IVideoDriver* driver;
  IrrSprite* loadSpriteFromJSON(json json_data, std::string directory);
public:
  SpriteLoader(IVideoDriver* driver);  
  IrrSprite* loadSprite(const wchar_t* file, std::vector<GameRect> frameClips);
  IrrSprite* loadSpriteFromString(std::string json_data, std::string directory);
  IrrSprite* loadSpriteFromFile(std::string json_file, std::string directory);

};
#endif
