#ifndef GAMECORE
#define GAMECORE

struct GameRect{
  int x1;
  int y1;
  int x2;
  int y2;
};

struct Vector2i{
  int x;
  int y;
};

#endif
